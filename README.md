# JavaScript Functions

## Tasks

#### Composition
Return the function composition of two specified functions f(x) and g(x).
The result of compose is to be a function of one argument, (lets call the argument x),
which works like applying function f to the result of applying function g to x, i.e. getComposition(f,g)(x) = f(g(x)).
For example:
```js
getComposition(Math.sin, Math.asin)(x) => Math.sin(Math.acos(x))
```
Write your code in `src/index.js` within an appropriate function `getComposition`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Power function
Return the math power function with the specified exponent.
For example:
```js
var power2 = getPowerFunction(2); // => x^2
power2(2) => 4
power2(4) => 16

var power05 = getPowerFunction(0.5); // => x^0.5
power05(4) => 2
power05(16) => 4
```
Write your code in `src/index.js` within an appropriate function `getPowerFunction`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Polynom
Returns the [polynom](https://en.wikipedia.org/wiki/Polynomial#Definition) function of one argument based on specified coefficients.
For example:
```js
getPolynom(2,3,5) => y = 2*x^2 + 3*x + 5
getPolynom(1,-3)  => y = x - 3
getPolynom(8)     => y = 8
getPolynom()      => null
```
Write your code in `src/index.js` within an appropriate function `getPolynom`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Memoize
Memoize passed function and return function which invoked first time calls the passed function and then always returns cached result.
For example:
```js
var memoizer = memoize(() => Math.random());
memoizer() => some random number  (first run, evaluates the result of Math.random())
memoizer() => the same random number  (second run, returns the previous cached result)
...
memoizer() => the same random number  (next run, returns the previous cached result)
```
Write your code in `src/index.js` within an appropriate function `memoize`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-functions
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-functions/  
4. Go to folder `javascript-functions`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-functions)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
